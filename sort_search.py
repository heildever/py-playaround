def bubble_sort(nums: list) -> list:
    ctr = 0
    for i in range(len(nums)-1):
        for j in range(len(nums)-1-i):
           if nums[j] > nums[j+1]:
               nums[j], nums[j+1] = nums[j+1], nums[j]
               ctr+=1
    print(ctr)
    return nums


def quick_sort(nums: list) -> list:
    import statistics
    # best pivot is the median of the list
    pivot = int(statistics.median(nums)) # this line can be avoided
    lesser = [i for i in nums if i < pivot]
    greater = [i for i in nums if i > pivot]
    return lesser+[pivot]+greater

def inplace_quicksort(nums: list) -> list:
    def _quicksort(nums, low, hi):
        if low< hi:
            p = partition_qs(nums, low, hi)
            _quicksort(nums, low, hi)
            _quicksort(nums, p+1, hi)
    def partition_qs(nums, low, hi):
        pivot = nums[low]
        while True:
            while low<pivot:
                low += 1
            while hi>pivot:
                hi -= 1
            if hi <= low:
                return hi
            nums[low], nums[hi] = nums[hi], nums[low]
            low += 1
            hi -= 1
    return nums

def inplace_mergesort(nums: list) -> list:
    if len(nums)<2:
        return nums
    mid = len(nums)//2
    left = inplace_mergesort(nums[:mid])
    right = inplace_mergesort(nums[mid:])
    def merge(left, right):
        result = []
        i = j = 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                result.append(left[i])
                i += 1
            else:
                result.append(right[j])
                j += 1
        result += left[i:]
        result += right[j:]
        return result

    return merge(left, right)

def ite_bin_search(nums: list, num: int) -> int:
    if num in nums:
        lo, hi = 0, len(nums) - 1
        while hi >= lo:
            mid = (lo + hi)//2
            if num > nums[mid]:
                lo = nums[mid]
            elif nums[mid] > num:
                hi = nums[mid]
            else: return lo

def rec_bin_search(nums: list,  num: int, lo: int, hi: int) -> int:
    if num in nums:
        if hi >= lo:
            mid = (lo + hi)//2
            if num > nums[mid]:
                return(rec_bin_search(nums, num, mid+1, hi))
            elif nums[mid] > num:
                return(rec_bin_search(nums, num, lo, mid-1))
            else:
                return lo
    else: return None

if __name__ == "__main__":
    pass
