import unittest
import random
import listexc


class Test_Listexc(unittest.TestCase):
    """docstring forTest_Listexc.unittest.TestCase __init__(self, arg):
        superTest_Listexc,unittest.TestCase.__init__()
        self.arg = arg
        """

    def test_largest_two(self):
        arr = [1,-1,2,5,-4,12,-16,26,-52,16,31,-58]
        max1 = sorted(arr)[0]
        if abs(sorted(arr)[1]) > abs(sorted(arr)[-1]):
            max2 = sorted(arr)[1]
        else:
            max2 = sorted(arr)[-1]
        self.assertEqual(listexc.largest_two(arr), (max1, max2))

if __name__ == '__main__':
    unittest.main()
