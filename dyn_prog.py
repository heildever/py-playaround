def fibo_ite(n: int) -> int:
    if n <= 0:
        raise ValueError
    else:
        seq = [0, 1, 1]
        for i in range(3, n+1):
            seq.append(seq[i-1] + seq[i-2])
    return seq[n-1]

def fibo_rec(n: int):
    if n == 1 or n == 2:
        return 1
    elif n > 2:
        return fibo_rec(n-1) + fibo_rec(n-2)

def recursive_staircase(num: int) -> int:
    # this problem can be mapped to a Fibonacci sequence
    if num <= 0:
        raise ValueError
    else:
        seq = [1, 2]
    for i in range(2, num+1):
        seq.append(seq[i-1] + seq[i-2])
    return seq[num]

def longest_common_prefix(strs: list) -> str:
    # brute force method
    subs = ''
    for i in range(0, len(strs[0])):
        if all(j.startswith(strs[0][:i+1]) for j in strs[1:]):
            subs += strs[0][i]
    return subs

def max_subarray(nums: list) -> int:
    ''' KADANE's
    if nums[i]>0 add it to nums[i+1] and extract the largest num from nums
    for i in range(1, len(nums)):
        if nums[i-1] > 0:
            nums[i] += nums[i-1]
    return max(nums)
    '''
    local_max = global_max = nums[0]
    for i in nums[1:]:
        local_max = max(i, local_max+i)
        if local_max > global_max:
            global_max = local_max
    return global_max

def max_profit(prices: list) -> int: # stock buy and sell problem

    # can be approached with kadane's algorithm
    ''' brute force solution
    profit = 0
    for i in nums:
        for j in nums[nums.index(i)+1:]:
            if j-i>0 and j-i>profit:
                profit = j-i
    return(profit)
    '''
    max_profit = min_ind = 0
    for i in range(1, len(prices)):
        if prices[i] < prices[min_ind]:
            min_ind = i
        max_profit = max(max_profit, prices[i] - prices[min_ind])
    return max_profit

def power(base, expo):
    if expo > 1:
        return base * power(base, expo-1)
    elif expo == 1:
        return base
    elif expo == 0:
        return 0

if __name__ == "__main__":
    print(power(2,3))