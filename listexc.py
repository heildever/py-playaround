

def make_anagram(seq1: str, seq2: str) -> int:
    import collections
    col_a = collections.Counter(seq1)
    col_b = collections.Counter(seq2)
    common = col_a & col_b
    return sum(col_a.values()) + sum(col_b.values()) - 2*sum(abs(i) for i in common.values())


def sherlock_str(arg: str) -> bool:
    import collections
    arg = collections.Counter(arg)
    arg = [i for i in arg.values() if i > 2]
    return 'true ' if  arg != [] else 'false'
    # return all(i for i in arg.values() if 1 <=i < 3)


def print_til_int(num: int):
    for i in range(num):
        print(i, end='')


def largest_two(arr: list):
    maks = meks = arr[0]
    for i in arr[1:]:
        if abs(i) > abs(maks):
            maks = i
        elif abs(maks) > abs(i) > abs(meks):
            meks = i
    return maks, meks


def convert_case(xtr: str) -> str:
    s1 = '' #create a new string
    for i in xtr:
        if i.isupper():
            s1 += i.lower()
        else:
            s1 += i.upper()
    return s1


def fact_ite(num: int, init: int=1) -> int:
    for i in range(num+1):
        init *= i
    return init


def fact_rec(num: int) -> int:
    if num > 1:
        num = num * fact_rec(num-1)
    return num


def removeIntfromList(nums: list, val: int) -> list:
    # remove 'val' from list
    nums[:] = [nums.remove(i) for i in nums if i==val]
    # create a new list without the 'val'
    # nums[:] = [i for i in nums if i!=val]
    # can be solved with filter(lambda())
    return nums


def isLeap(year: int, leap: bool=False) -> bool:
    if 1900<=year<=10**5 and year%4==0:
        leap = False if year%100==0 and year%400!=0 else True
    return leap


def isAnagram(p: str ,q: str) -> bool:
    return(sorted(p)==sorted(q))


def isPalindrome(s: str) -> bool:
    s = ''.join(e for e in s if e.isalnum()).lower()
    return s==s[::-1]


def fizz_buzz(n: int, ls=[]):
    for i in range(1, n+1):
        if i%3==0:
            ls.append('Fizz')
        elif i%5==0 and i%3!=0:
            ls.append('Buzz')
        elif i%3==0 and i%5==0:
            ls.append('FizzBuzz')
        else:
            ls.append(str(i))
    return print(ls)


n = 5
A = [2, 3, 6, 6 ,5]
try:
    2<=n<=10 and all(-100<=i<=100 for i in A)
    ### sol 1
    # A[:] = [x for x in A if x<max(A)]
    # print(max(A))
    ### sol2
    # print(max(set(filter(lambda x: x<max(A), A))))
    ### sol 3
    # A[:] = set(A)
    # A.remove(max(A))
    # print(max(A))
except ValueError:
    print('nabuyun amk')

xtr = 'z'
A = [i for i in str(xtr)]
# if any(1<A.count(i) for i in A):
    # B = list(filter(lambda x: A.count(x)<2, A))
    # print(len(B))
    # print(A.index(B[0]))
# else:
    # print(-1)

s = 'leetcode'
p = [x for x in set(s) if s.count(x)==1]
# if len(p)!=0:
    # print(s.index(p[0]))
# else:
    # print -1
# nums[:] = [int(i) for i in nums if nums.count(i)==1]
# lambda x: nums.count(x)==1, nums

s = 'aa'
t = 'ab'
m = dict((k,s.count(k)) for k in s)
r = dict((k,t.count(k)) for k in t)
if len(m.keys())==len(r.keys()) and set(m.values())==set(r.values()):
    output_dict = dict(zip(s, t))
    for i in range(len(s)):
        if (s[i],t[i]) not in output_dict.items():
            print ('babayarro')
else:
    pass
    # print('yaraaak')

s = ''
# print(min([s.index(char) for char in set(s) if s.count(char) == 1] or [-1]))
seen = set()
for i in s:
    if i in seen:
        print(s.index(i))
    else:
        seen.add(i)

nums1 = [4,9,5]
nums2 = [9,4,9,8,4]
# naive --- common = [i for i in nums1 if i in nums2] # print(common)
#print(list(set(nums1) & set(nums2)))
#if len(nums1)<=len(nums2):
    #print(list(set(nums2).intersection(set(nums1))))
#else:
    #print(list(set(nums1).intersection(set(nums2))))

nums = [1, 1]
# if nums:
    # n = [i for i in range(1, len(nums)+1) if i not in nums]
# else:
    # n = []
arr = [i for i in range(1, len(nums)+1)]
diff = set(arr).difference(nums)

x = "AABAAB"
i=0
for counter, character in enumerate(x, 1):
    if len(x)>counter and character == x[counter]:
        i+=1
        #  print character, x[counter]
# print i

def rev_int(num: int) -> int:
    i = int(''.join(list(str(abs(num)))[::-1]))
    if neg_max < i < pos_max:
        return(i if x>0 else i*-1)
    else:
        return i


def array_intersect(nums1: list, nums2: list) -> list:
    import collections
    col_a = collections.Counter(nums1)
    col_b = collections.Counter(nums2)
    common = col_a & col_b
    return[i for i in common.elements()]


def atoi(seq: str) -> int:
    import re
    seq = seq.strip() # strips both leading and trailing whitespace chars
    seq = re.findall(r'^[+\-]?\d+', seq)
    if seq:
        seq = int(''.join(seq))
        return max(neg_max, min(seq, pos_max))
    else:
        return 0


def strStr(haystack: str, needle: str) -> int:
    return haystack.index(needle) if needle in haystack else 0


def bitwise_add(num1, num2):
    """til carry runs out
       find the carry                   carry = x & y
       a XOR b // the actual addition   x = x ^ y
       b stores left shifted carry      y = carry << 1
    """
    mask = 0xFFFFFFFF
    while num2 != 0:
        num1 = (num1 ^ num2) & mask
        num2 = ((num1 & num2) << 1) & mask
    # if a is negative, get a's 32 bits complement positive first
    # then get 32-bit positive's Python complement negative
    return num1 if neg_max < num1 < pos_max else ~(num1 ^ mask)


def count_primes(num: int) -> int:
    if n < 3:
        return 0
    primes = [1] * n
    primes[0] = primes[1] = 0
    for i in range(2, int(n ** 0.5) + 1):
        if primes[i]:
            primes[i * i: n: i] = [0] * len(primes[i * i: n: i])
    # return sum(primes)
    # golfing
    return len([i for i in range(2, num) if all(i%j!=0 for j in range(2, int(i ** 0.5)+1))])


def ransomnote_dict(magazine: str, note: str) -> bool:
    note_dict = {k:note.split(" ").count(k) for k in note.split(" ")}
    mag_dict = {k:magazine.split(" ").count(k) for k in magazine.split(" ")}
    return True if all(k in mag_dict.keys() and mag_dict.get(k) >= v for k, v in note_dict.items()) else False


def rev_matrix(matrix: list) -> list:
    return list(zip(*(matrix[::-1])))


def countNsay(num: str) -> str:
    import collections
    num = list(''.join(num))
    num = collections.Counter(num)
    aux = [k+str(num[k]) for k in num.keys()]
    return ''.join(aux)


def missing_num(nums: list):
    # Gauss's formula
    # return int(len(nums) * (len(nums)+1) / 2 - sum(nums))
    # XOR
    missing = len(nums)
    for idx, num in enumerate(nums):
        missing ^= idx ^ num
    return missing


def non_repeat_num(nums: list) -> int:
    res = 0
    for i in nums:
        res ^= i
    return res


def missing_recurring(nums: list) -> list:
    s = (1+len(nums))*len(nums)/2
    missing = s- sum(set(nums))
    recurring = sum(nums)- (1+len(nums))*len(nums)/2+missing
    return [missing, recurring]


def frequency_sort(seq: str) -> str:
    import collections
    seq = collections.Counter(seq)
    return(''.join(k*v for k,v in seq.most_common()))


if __name__ == "__main__":
    global neg_max
    global pos_max
    neg_max, pos_max = -0x80000000, 0x7FFFFFFF
